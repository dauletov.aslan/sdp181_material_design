package kz.astana.materialdesign;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DrawerLayout drawerLayout = findViewById(R.id.drawerLayout);
        NavigationView navigationView = findViewById(R.id.navigationView);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(navigationView);
            }
        });

        ImageView imageView = findViewById(R.id.imageView);
        LinearLayout linearLayout = findViewById(R.id.linearLayout);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if (item.getItemId() == R.id.first) {
                    imageView.setImageResource(R.drawable.ic_first);
                } else if (item.getItemId() == R.id.second) {
                    imageView.setImageResource(R.drawable.ic_second);
                } else if (item.getItemId() == R.id.third) {
                    imageView.setImageResource(R.drawable.ic_third);
                    startActivity(new Intent(MainActivity.this, SecondActivity.class));
                }
                drawerLayout.closeDrawers();
                return true;
            }
        });

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                fab.hide();
//                fab.show();
                Snackbar
                        .make(
                                linearLayout,
                                "This is Snackbar",
                                Snackbar.LENGTH_INDEFINITE
                        )
                        .setAction(
                                "Hello",
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Toast.makeText(MainActivity.this, "This is Toast", Toast.LENGTH_SHORT).show();
                                    }
                                }
                        )
                        .show();
            }
        });
    }
}